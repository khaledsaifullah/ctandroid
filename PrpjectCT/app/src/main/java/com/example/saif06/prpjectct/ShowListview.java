package com.example.saif06.prpjectct;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.StringDef;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

/**
 * Created by Saif06 on 1/7/17.
 */

public class ShowListview extends AppCompatActivity {
    String countryName[] = {"Exit From Application", "Bangladesh", "Nepal", "Srilangka", "Poland", "Sweden", "Canada", "USA"
            , "Mexico", "UAE", "Russia", "Norway", "Finland", "South Africa", "Hungery", "Brazil"};
    ListView list;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);
        list = (ListView) findViewById(R.id.list);
        ArrayAdapter<String> adapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, countryName);
        list.setAdapter(adapter);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long l) {
                /*String a=String.valueOf(parent.getItemAtPosition(position) );
                Toast.makeText(ShowListview.this, a, Toast.LENGTH_SHORT).show();
*/
               if (position == 0) {
                    onBackPressed();
                } else {
                    Toast.makeText(ShowListview.this, countryName[position], Toast.LENGTH_SHORT).show();
                }


            }
        });
    }

    @Override
    public void onBackPressed() {

        final AlertDialog.Builder alert = new AlertDialog.Builder(ShowListview.this);
        alert.setTitle("Exit From Application");
        alert.setMessage("Are you sure ? ");
        alert.setCancelable(false);
        alert.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                finish();
            }
        });
        alert.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        alert.show();

    }
}

