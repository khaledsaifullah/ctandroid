package com.example.saif06.prpjectct;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.StringDef;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

/**
 * Created by Saif06 on 2/9/17.
 */

public class LogIn extends AppCompatActivity {
    EditText et_UserName, et_passWord;
    Button btn_logIn;
    String preference_file_key="app_pref";


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Context context=this;

        //readSession();

        setContentView(R.layout.activity_log_in);

        final String userName = "Saif";
        final String passWord = "123456";

        et_UserName = (EditText) findViewById(R.id.et_UserName);
        et_passWord = (EditText) findViewById(R.id.et_passWord);
        btn_logIn = (Button) findViewById(R.id.btn_logIn);

        btn_logIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (et_UserName.getText().toString().equals(userName) && et_passWord.getText().toString().equals(passWord)) {

                    SharedPreferences sharedPref =getSharedPreferences(
                            Splash.preference_file_key, Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedPref.edit();
                    editor.putString("LOGIN", "true");
                    editor.commit();


                    startActivity(new Intent(LogIn.this, TheNextActivity.class));
                }
                else {
                    Toast.makeText(LogIn.this, "User Name and Password didn't Match", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    /*private void readSession() {

        SharedPreferences sharedPref =getSharedPreferences(
                preference_file_key, Context.MODE_PRIVATE);
       // boolean isLoggedIn=sharedPref.getBoolean("LOGIN",false);
        if(sharedPref.getBoolean("LOGIN",false))
        {
            startActivity(new Intent(LogIn.this, TheNextActivity.class));
            finish();
        }


    }*/
}
