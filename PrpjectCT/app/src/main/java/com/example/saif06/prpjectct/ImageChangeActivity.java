package com.example.saif06.prpjectct;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

/**
 * Created by Saif06 on 1/14/17.
 */

public class ImageChangeActivity extends AppCompatActivity {
    private ImageView img;
    private Button btn_change_image;
    int countar = 1;
    Toolbar toolbar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_change);

        toolbar=(Toolbar)findViewById(R.id.toolbar);

        toolbar.setTitle("CodersTrust");
        toolbar.setSubtitle("In code we trust");

        setSupportActionBar(toolbar);
       // getSupportActionBar().setHomeButtonEnabled(true);

        img = (ImageView) findViewById(R.id.img);
        btn_change_image = (Button) findViewById(R.id.btn_change_image);

        btn_change_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //img.setImageResource(R.drawable.la_three);
                if (countar == 0) {
                    img.setImageResource(R.drawable.la_one);
                    countar++;
                } else if (countar == 1) {
                    img.setImageResource(R.drawable.la_two);
                    countar++;
                } else if (countar == 2) {
                    img.setImageResource(R.drawable.la_three);
                    countar = 0;
                } else {

                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater inflater=getMenuInflater();
        inflater.inflate(R.menu.menu,menu);
        return super.onCreateOptionsMenu(menu);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId())
        {
            case R.id.about:
                break;
            case  R.id.contact:
                break;
            default:
                break;
        }
        return true;
    }
}
