package com.example.saif06.prpjectct;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

/**
 * Created by Saif06 on 12/31/16.
 */

public class TheNextActivity extends AppCompatActivity {
    Button  btn_logOut;

    String preference_file_key="app_pref";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_next);

        btn_logOut=(Button)findViewById(R.id.btn_logOut);
        btn_logOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences sharedPref =getSharedPreferences(
                        Splash.preference_file_key, Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putString("LOGIN", "false");
                editor.commit();

               startActivity(new Intent(TheNextActivity.this,LogIn.class));
            }
        });


    }
}
