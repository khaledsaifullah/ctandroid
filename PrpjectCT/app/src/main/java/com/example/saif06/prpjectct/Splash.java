package com.example.saif06.prpjectct;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.widget.Toast;

/**
 * Created by Saif06 on 2/9/17.
 */

public class Splash extends AppCompatActivity{
    public static final String preference_file_key="app_pref";
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        SharedPreferences sharedPref =getSharedPreferences(
                preference_file_key, Context.MODE_PRIVATE);

        String isLoggedIn=sharedPref.getString("LOGIN","");



        if(isLoggedIn.equals("true"))
        {
            startActivity(new Intent(Splash.this, TheNextActivity.class));
            finish();
        }
        else{
            startActivity(new Intent(Splash.this, LogIn.class));
            finish();
        }
    }
}
