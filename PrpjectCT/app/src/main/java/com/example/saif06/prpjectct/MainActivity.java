package com.example.saif06.prpjectct;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.ButtonBarLayout;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    Button btnNext;
    EditText userName,passWord;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log_in);
        userName=(EditText)findViewById(R.id.et_UserName);
        passWord=(EditText)findViewById(R.id.et_passWord);
        btnNext=(Button)findViewById(R.id.btn_next);
        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(userName.getText().toString().equals("")||
                passWord.getText().toString().equals(""))
                {
                    Toast.makeText(MainActivity.this, "Please fill up all the fields first",Toast.LENGTH_SHORT).show();
                }
               else{
                    String userNameTemporary=userName.getText().toString();
                    Intent i = new Intent(MainActivity.this, TheNextActivity.class);
                    i.putExtra("KEY_USER",userNameTemporary);
                    startActivity(i);
                }

            }
        });
    }
}
